#!/usr/bin/python

import urllib
import urllib2
import json
import os
import sys

if True:
  dest_dir = './vid_test'
  #= 'https://liveredbulltv02-i.akamaihd.net/hls/live/502328/170430UCIfrWomPri/master_3360.m3u8'
  #largest_bandwidth_url = 'https://liveredbulltv02-i.akamaihd.net/hls/live/504132/170604UCImenPri/master_3360.m3u8'  
  largest_bandwidth_url = 'http://liveredbulltv02-i.akamaihd.net/hls/live/504134/170604UCIrestr01/master.m3u8'
  f = urllib.urlopen(largest_bandwidth_url)
  rendition_list = f.read()

  command = 'avconv -i "concat:'

  for line in rendition_list.split('\n'):
    if line.find('https://') > -1:
      filename = line.split('/')[-1].split('#')[0].split('?')[0]
      #command += filename + "|"
      print(line)
      try:
        req = urllib2.urlopen(line)
        CHUNK = 16 * 1024
        with open(os.path.join(dest_dir, filename), 'wb') as fp:
          while True:
            chunk = req.read(CHUNK)
            if not chunk: break
            fp.write(chunk)
        command += filename + "|"
      except:
        print "EXCEPTION: " + line

  command = command[0:-1] + '" -c copy output.ts'
  # Save command rather than spitting out?
  command_file_name = os.path.join(dest_dir, 'concat.sh')
  with open(command_file_name, 'w') as fp:
    fp.write(command)

  print "Done" 

