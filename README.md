# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Redbull TV Downloader
* -0.1

### Installation ###
1. Grab the two python scripts
2. install avconv

* e.g. sudo apt-get install avconv

### Instructions ###

1. Use search.py to find the ID of the stream you want:
* e.g. python search.py "Fort William"
* returns 2 lines per video found:
    [event id] Title - Description
    ...status.... url to api for full status information

* e.g.

    [event-stream-513] Downhill Finals - UCI MTB World Cup 2015: Fort William, UK
    ....pre-event....https://api.redbull.tv/v1/videos/event-stream-513/status
    [event-stream-356] Downhill FInals - UCI MTB World Cup 2014: Fort William, UK
    ....replay....https://api.redbull.tv/v1/videos/event-stream-356/status

2. Update fetch_and_prep.py to point to the directory you want to download the files to, find line starting with 'dest_dir' and update:

* e.g. dest_dir = '/media/thumb1'

3. Use the event id with fetch_and_prep.py to fetch all the chunks from the stream and to create a shell script to stitch the files together to create one video. 
*Note:* will only work for videos with a status of 'replay'.

* e.g. python fetch_and_prep.py event-stream-356