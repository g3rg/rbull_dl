#!/usr/bin/env python2

import urllib
import urllib2
import json
import os
import sys

if len(sys.argv) < 2:
  print "You must pass an Event ID"
else:
  id = sys.argv[1]
  dest_dir = sys.argv[2]

  url_event_detail = "https://api.redbull.tv/v1/videos/"

  f = urllib.urlopen(url_event_detail + id)
  response = json.loads(f.read())

  m3u8_url = ''
  if 'demand' in response['videos']:
    m3u8_url = response['videos']['demand']['uri']
  elif 'master' in response['videos']:
    m3u8_url = response['videos']['master']['uri']

  print m3u8_url 

  f = urllib.urlopen(m3u8_url)
  master_list = f.read()

  largest_bandwidth=0
  largest_bandwidth_url=""
  new_largest = True

  for line in master_list.split('\n'):
    x = line.find('BANDWIDTH=')
    if x > -1:
      x = x+len('BANDWIDTH=')
      y = line[x:].find(',') + x
      bandwidth = int(line[x:y])
      if bandwidth > largest_bandwidth:
        largest_bandwidth = bandwidth
        new_largest = True
    else:
      x = line.find('http://')
      if x > -1 and new_largest:
        largest_bandwidth_url = line
        new_largest = False


  f = urllib.urlopen(largest_bandwidth_url)
  rendition_list = f.read()

  command = 'avconv -i "concat:'

  for line in rendition_list.split('\n'):
    if line.find('http://') > -1:
      filename = line.split('/')[-1].split('#')[0].split('?')[0]
      #command += filename + "|"
      print(line)
      try:
        req = urllib2.urlopen(line)
        CHUNK = 16 * 1024
        with open(os.path.join(dest_dir, filename), 'wb') as fp:
          while True:
            chunk = req.read(CHUNK)
            if not chunk: break
            fp.write(chunk)
        command += filename + "|"
      except:
        print "EXCEPTION: " + line

  command = command[0:-1] + '" -c copy output.ts'
  # Save command rather than spitting out?
  command_file_name = os.path.join(dest_dir, 'concat.sh')
  with open(command_file_name, 'w') as fp:
    fp.write(command)

  print "Done" 

