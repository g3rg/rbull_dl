#! /usr/bin/env python2
import json
import sys
import urllib

if len(sys.argv) < 2:
  print("No search criteria provided")
else:
  searchCriteria = sys.argv[1]
  url_event_stream = "https://api.redbull.tv/v1/videos/event_streams?"
  url_clip = "https://api.redbull.tv/v1/videos/clips?"
  
  url_search_param = "search"

  params = {}
  params[url_search_param] = searchCriteria
  params = urllib.urlencode(params)

  urls = []
  urls.append(url_event_stream)
  urls.append(url_clip)

  results = []

  for url in urls:
    f = urllib.urlopen(url + params)
    response = json.loads(f.read())

    for vid in response['videos']:
      result = {}
      result['id'] = vid['id']
      result['title'] = vid['title'] + " - " + vid['subtitle']
      result['description'] = vid['long_description']
      result['status'] = 'clip' #vid['meta'] #['status']
      result['date'] = vid['published_on']
      result['subtitle'] = vid['title']
      result['duration'] = vid['duration']
      result['published_on'] = vid['published_on']
            
      if vid['stream'] is None:
        result['status'] = 'clip' #vid['meta'] #['status']
      else:
        result['status'] = vid['stream']['status']
    
    
      output = "[" + result['id'] + "] " + result['title'] + " - " + result['subtitle'] + "\n"
      output = output + "[Duration:" + result['duration'] + "] [Published:" + result['published_on'] + "]\n"
      output = output + "...." + result['status']
      #output = output + "...." + result['meta']['links']['status']
      print(output)

  
#    for vid in response['videos']:
#      # If there is no stream URL then exit cleanly
#      if vid['stream'] is None:
#        continue;
#      output = "[" + vid['id'] + "] " + vid['title'] + " - " + vid['subtitle'] + "\n"
#      output = output + "...." + vid['stream']['status']
#      output = output + "...." + vid['meta']['links']['status']
#      #id, type, title, subtitle, short_description, long_description, duration
#      #published_on, images, show/meta/links[self/episodes/status],
#      print(output)
