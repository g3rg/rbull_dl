import os
import urllib
import json

import celery

from flask import render_template, request, flash, Response
from flask.ext.mail import Message, Mail
from flask.ext.wtf import Form
from wtforms import TextField, SubmitField, validators

from app import app

mail = Mail()

@app.route('/')
def home():
  return render_template('home.html')

class RedbullForm(Form):
  searchCriteria = TextField('Search Criteria', [validators.Required("Please enter a search criteria.")])
  
  submit = SubmitField("Search")

@app.route('/redbull_detail/<id>', methods=['GET'])
def redbull_detail(id):
  url = "https://api.redbull.tv/v1/videos/" + id
  
  f = urllib.urlopen(url)
  response = json.loads(f.read())
  m3u8_url = ''
  if 'demand' in response['videos']:
    m3u8_url = response['videos']['demand']['uri']
  elif 'master' in response['videos']:
    m3u8_url = response['videos']['master']['uri']
  
  f = urllib.urlopen(m3u8_url)
  master_list = f.read()
  
  largest_bandwidth=0
  largest_bandwidth_url=""
  new_largest = True
  output = ""
  
  for line in master_list.split('\n'):
    x = line.find('BANDWIDTH=')
    if x > -1:
      x = x+len('BANDWIDTH=')
      y = line[x:].find(',') + x
      bandwidth = int(line[x:y])
      if bandwidth > largest_bandwidth:
        largest_bandwidth = bandwidth
        new_largest = True
    else:
      x = line.find('http://')
      if x > -1 and new_largest:
        largest_bandwidth_url = line
        new_largest = False

  f = urllib.urlopen(largest_bandwidth_url)
  rendition_list = f.read()

  ffmpeg_command = 'ffmpeg -i "concat:'
  fetch_command = ''

  for line in rendition_list.split('\n'):
    if line.find('http://') > -1:
      filename = line.split('/')[-1].split('#')[0].split('?')[0]
      ffmpeg_command += filename + "|"
      fetch_command += 'wget ' + line + ' -O ' + os.path.join(app.config['REDBULL_DEST_DIR'], filename) + "\n"

  ffmpeg_command = ffmpeg_command[0:-1] + '" -vcodec copy -acodec copy output.ts'
  background_script.delay('/media/prod_thumb/test.sh', fetch_command + '\n\n' + ffmpeg_command)
  return "Scheduled"

  #return Response(fetch_command + "\n\n" + ffmpeg_command, mimetype='text/plain')
  # Save command file
  # schedule cron?
  

@app.route('/redbull', methods=['GET', 'POST'])
def redbull():
  form = RedbullForm()
  results = []

  if request.method == 'POST':
    if form.validate():
      results = search_redbull(form.searchCriteria.data)
    else:
      flash('All fields are required.')

    return render_template('redbull.html', form=form, results=results)
  elif request.method == 'GET':
    return render_template('redbull.html', form=form, results=results)

@app.route('/youtube')
def youtube():
  return render_template('youtube.html')

@app.route('/podcast')
def podcast():
  return render_template('podcast.html')
@app.route('/debug')
def debug():
  return "testing 1 2 3"
  
def search_redbull(searchCriteria):
  url_event_stream = "https://api.redbull.tv/v1/videos/event_streams?"
  url_clip = "https://api.redbull.tv/v1/videos/clips?"
  
  url_search_param = "search"

  params = {}
  params[url_search_param] = searchCriteria
  params = urllib.urlencode(params)

  urls = []
  urls.append(url_event_stream)
  urls.append(url_clip)

  results = []

  for url in urls:
    f = urllib.urlopen(url + params)
    response = json.loads(f.read())

    for vid in response['videos']:
      result = {}
      result['id'] = vid['id']
      result['title'] = vid['title'] + " - " + vid['subtitle']
      result['description'] = vid['long_description']
      result['status'] = 'clip' #vid['meta'] #['status']
      result['date'] = vid['published_on']
            
      if vid['stream'] is None:
        result['status'] = 'clip' #vid['meta'] #['status']
      else:
        result['status'] = vid['stream']['status']
    
      results.append(result)

  return results

@celery.task
def background_script(filename, script):
  with open(filename, 'w') as fp:
    fp.write(script)
  return 'done'
  