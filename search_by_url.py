#!/usr/bin/python

import urllib
import urllib2
import json
import os
import sys
import re

if len(sys.argv) < 2:
  print "You must pass an Event URL and a destination folder"
else:
  src_url = sys.argv[1]
  dest_dir = sys.argv[2]

  f = urllib.urlopen(src_url)
  response = f.read()

  script_start = "<script>"
  script_end = "</script>"

  elements = re.findall(r'https:(.*?).m3u8', response)

  for ele in elements:
  	print ele
  	print "\n\n"

  
